%include "words.inc"

%define BUFF_SIZE 256
%define PSIZE 8

section .bss
buffer resb BUFF_SIZE

section .data
error_key_not_found db "Key not found", 0
error_key_too_long db "Key must have < 255 characters).", 0

section .text
global _start

extern read_word
extern find_word
extern string_length
extern print_string
extern print_error
extern exit

_start:
    mov rdi, buffer
    mov rsi, BUFF_SIZE
    call read_word

    test rax, rax
    jz .key_too_long

    mov rdi, rax
    mov rsi, last
    call find_word

    test rax, rax
    jz .key_not_found

    mov rdi, rax
    add rdi, PSIZE
    push rdi
    call string_length

    pop rdi
    lea rdi, [rdi+rax+1]
    call print_string

    jmp .exit

.key_too_long:
    mov rdi, error_key_too_long
    call print_error
    jmp .exit
    
.key_not_found:
    mov rdi, error_key_not_found
    call print_error

.exit:
    xor rdi, rdi
    call exit
