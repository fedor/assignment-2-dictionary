%define SYS_EXIT 60
%define SPACE 0x20
%define HORIZONTAL_TAB 0x9
%define SUBSTITUTION 0xA
section .text

global exit
global string_length
global print_string
global print_error
global print_newline
global print_char
global print_int
global print_uint
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte[rdi + rax], 0
        je .return 
        inc rax
        jmp .loop
    .return:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
   push rdi
   call string_length
   pop rsi
   mov rdx, rax
   mov rax, 1 
   mov rdi, 1
   syscall
   ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rsi, rsp
    mov rdi, 1
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит ошибку
print_error:
    push rdi
    call string_length
    pop rsi
	mov rdx, rax
	mov rax, 1; код write system call
	mov rdi, 2; код error system call
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
xor rax, rax
    xor rcx, rcx
    dec rsp
    mov [rsp], al
    mov rax, rdi
    mov r8, 10
    .loop:
        inc rcx
        xor rdx, rdx
        div r8
        add rdx, '0'
        dec rsp
        mov [rsp], dl
        test rax, rax
        jnz .loop
        mov rdi, rsp
        push rcx
        call print_string
        pop rcx
        add rsp, rcx
        inc rsp
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi,0
    jl .neg
    call print_uint
    ret
    .neg:
    	push rdi
    	mov rdi,'-'
    	call print_char
    	pop rdi
    	neg rdi
    	call print_uint		
    	ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    .loop:
        mov cl, [rdi]
        mov dl, [rsi]
        cmp cl, dl
        jne .end
        inc rdi
        inc rsi
        dec rax
        cmp rax, -1
        jne .loop
        mov rax, 1
        ret
    .end:
        mov rax, 0
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rdi, rdi              
    xor rax, rax
    mov rsi, rsp                   
    mov rdx, 1            
    syscall
    pop rax                 
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    mov r10, rdi
    mov r9, rsi
    mov rsi, rdi
    mov rdi, 0
    mov rdx, 1
    .delete:
        mov rax, 0
        syscall
        test rax, rax
        je .zero
        mov al, [rsi]
        cmp rax, 0x20
        je .delete
        cmp rax, 0x9
        je .delete
        cmp rax, 0xA
        je .delete
    inc rsi
    mov rdi, 0
    mov rdx, 1
    .iterate:
        mov rax, 0
        syscall
        cmp rax, -1
        je .error
        test rax, rax
        je .end
        mov al, [rsi]
        cmp rax, 0x20
        je .end
        cmp rax, 0x9
        je .end
        cmp rax, 0xA
        je .end
        dec r9
        test r9, r9
        je .error
        inc rsi
        jmp .iterate
    .end:
        mov byte[rsi], 0
        mov rax, r10
        sub rsi, r10
        mov rdx, rsi
        ret
    .error:
        mov rax, 0
        ret
    .zero: 
        mov rdx, rax
        mov rax, rsi
        ret
        
; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor rcx, rcx
    mov r10, 10
    .loop:
        mov cl, [rdi]
        sub cl, 48
        cmp cl, 9
        jg .end
        cmp cl, 0
        jl .end
        push rdx
        mul r10
        pop rdx
        add rax, rcx
        inc rdi
        inc rdx
        jmp .loop
    .end:
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi],'-'
    je .neg
    call parse_uint
    ret
    .neg:
    	inc rdi
    	call parse_uint
    	neg rax
    	inc rdx
    	ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    .loop:
        test rdx, rdx
        jz .overflow
        mov r8b, [rdi + rcx]
        mov [rsi + rcx], r8b
        test r8b, r8b
        jz .return
        inc rcx
        dec rdx
        jmp .loop
    .overflow:
        xor rcx, rcx
    .return:
        mov rax, rcx
        ret
