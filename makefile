ASM = nasm -felf64

clean:
	rm -f main.o dict.o lib.o programm

lib.o: lib.asm
	nasm -felf64 -o lib.o lib.asm

dict.o: dict.asm
	nasm -felf64 -o dict.o dict.asm

main.o: main.asm lib.o dict.o
	nasm -felf64 -o main.o main.asm

programm: main.o lib.o dict.o
	ld -o programm main.o lib.o dict.o

test: lib.asm dict.asm main.asm lib.o dict.o
	rm -f main.o dict.o lib.o programm
	nasm -felf64 -o lib.o lib.asm
	nasm -felf64 -o dict.o dict.asm
	nasm -felf64 -o main.o main.asm
	ld -o programm main.o lib.o dict.o
	python3 test.py	
