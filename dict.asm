%define word_size 8

%include "lib.inc"

global find_word

find_word:
    test rdi, rdi
    je .end_of_dict

.cycle:
    test rsi, rsi
    je .end_of_dict

    push rdi
    push rsi
    lea rsi, [rsi + word_size]

    call string_equals
    pop rsi
    pop rdi
    cmp rax, 1
    je .key_found

    mov rsi, [rsi]
    jmp .cycle

.key_found:
    xor rax, rsi
    jmp .exit

.end_of_dict:
    xor rax, rax
    jmp .exit

.exit:
    ret
