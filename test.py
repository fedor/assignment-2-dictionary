import subprocess
import os

test_inputs = ["first_word", "second_word", "third_word", "fourth_word", "loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooong"]

test_outputs = ["first word explanation", "second word explanation", "third word explanation", "", ""]

test_errors = ["", "", "", "Cannot find line in dictionary", "Unable to read word from user"]

print("\n")

tests = len(test_inputs);
failed_tests = 0;
correct_tests = 0;

for i in range(tests):
	p = subprocess.Popen(["./programm"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

	test_id = i + 1;
	
	out, err = p.communicate(input=test_inputs[i].encode())
	out = out.decode().strip()
	err = err.decode().strip()

	if (out == test_outputs[i] and err == test_errors[i]):
		correct_tests += 1;
		print("Test " + str(test_id) + ": successfull\n")
	else:
		failed_tests += 1;
		print("Test " + str(test_id) + ": failed")
		print("ERROR: " + test_errors[i])
		print("input: " + test_inputs[i])
		print("output: " + test_outputs[i] + "\n")

print("////////////////\nRESULT")
print("solved " + str(correct_tests) + " tests")
print("failed " + str(failed_tests) + " tests")
